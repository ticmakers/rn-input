# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Released

## [1.0.2] - 2020-01-30

### Fixed

- Fixed dependencies version

## [1.0.1] - 2019-04-26

### Fixed

- Fixed dependencies version

## [1.0.0] - 2019-04-05

### Release

# Unreleased

## [1.0.0-beta.1] - 2019-04-05

### Fixed

- Fix imports in Input component

## [1.0.0-beta.0] - 2019-04-05

### Added

- Upload and publish first version

[1.0.2]: https://bitbucket.org/ticmakers/rn-input/src/v1.0.2/
[1.0.1]: https://bitbucket.org/ticmakers/rn-input/src/v1.0.1/
[1.0.0]: https://bitbucket.org/ticmakers/rn-input/src/v1.0.0/
[1.0.0-beta.1]: #
[1.0.0-beta.0]: #
