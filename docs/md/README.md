
TIC Makers - React Native Input
===============================

React native component for input.

Powered by [TIC Makers](https://ticmakers.com)

Demo
----

Input Expo's snack

Install
-------

Install `@ticmakers-react-native/input` package and save into `package.json`:

NPM

```shell
$ npm install @ticmakers-react-native/input --save
```

Yarn

```shell
$ yarn add @ticmakers-react-native/input
```

How to use?
-----------

```javascript
import React from 'react'
import { AppLoading, Font } from 'expo'
import Input from '@ticmakers-react-native/input'

export default class App extends React.Component {
  userInput = null
  passInput = null

  render() {
    return (
      <View>
        <Input
          lang="es"
          label="Username"
          ref={ c => this.userInput = c }
          iconLeft={{ name: 'account-circle', type: 'material-community' }}
          rules={{ required: true, minLength: 3, maxLength: 8, email: true }}
        />

        <Input
          password
          label="Password"
          ref={ c => this.passInput = c }
          iconLeft={{ name: 'key', type: 'material-community' }}
          rules={{ required: true, minLength: 3, maxLength: 8 }}
        />
      </View>
    )
  }

  getValues() {
    return ({
      user: userInput.value,
      pass: passInput.value,
    })
  }
}
```

Properties
----------

Name

Type

Default Value

Definition

name

\-

\-

\-

Todo
----

*   Test on iOS
*   Improve and add new features
*   Add more styles
*   Improve readme (example & demo)
*   Create tests

Version 1.0.2 ([Changelog](https://bitbucket.org/ticmakers/rn-input/src/master/CHANGELOG.md))
---------------------------------------------------------------------------------------------

## Index

### External modules

* ["Input"](modules/_input_.md)
* ["index"](modules/_index_.md)
* ["styles"](modules/_styles_.md)

---

