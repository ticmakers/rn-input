[@ticmakers-react-native/input](../README.md) > ["Input"](../modules/_input_.md) > [Input](../classes/_input_.input.md)

# Class: Input

Input Component to write text, email, number and password

*__class__*: Input

*__extends__*: {ValidationComponent<IInputProps, IInputState>}

## Type parameters
#### SS 
## Hierarchy

 `ValidationComponent`<`IInputProps`, `IInputState`>

**↳ Input**

## Index

### Constructors

* [constructor](_input_.input.md#constructor)

### Properties

* [errors](_input_.input.md#errors)
* [fieldName](_input_.input.md#fieldname)
* [locale](_input_.input.md#locale)
* [messages](_input_.input.md#messages)
* [rules](_input_.input.md#rules)
* [value](_input_.input.md#value)

### Methods

* [IconError](_input_.input.md#iconerror)
* [IconEye](_input_.input.md#iconeye)
* [IconLeft](_input_.input.md#iconleft)
* [IconRight](_input_.input.md#iconright)
* [IconSuccess](_input_.input.md#iconsuccess)
* [Input](_input_.input.md#input)
* [Label](_input_.input.md#label)
* [MessageInput](_input_.input.md#messageinput)
* [UNSAFE_componentWillMount](_input_.input.md#unsafe_componentwillmount)
* [UNSAFE_componentWillReceiveProps](_input_.input.md#unsafe_componentwillreceiveprops)
* [UNSAFE_componentWillUpdate](_input_.input.md#unsafe_componentwillupdate)
* [_isComponent](_input_.input.md#_iscomponent)
* [_isProps](_input_.input.md#_isprops)
* [_onChangeValue](_input_.input.md#_onchangevalue)
* [_processProps](_input_.input.md#_processprops)
* [_toggleEye](_input_.input.md#_toggleeye)
* [componentDidCatch](_input_.input.md#componentdidcatch)
* [componentDidMount](_input_.input.md#componentdidmount)
* [componentDidUpdate](_input_.input.md#componentdidupdate)
* [componentWillMount](_input_.input.md#componentwillmount)
* [componentWillReceiveProps](_input_.input.md#componentwillreceiveprops)
* [componentWillUnmount](_input_.input.md#componentwillunmount)
* [componentWillUpdate](_input_.input.md#componentwillupdate)
* [getError](_input_.input.md#geterror)
* [getKeyboardType](_input_.input.md#getkeyboardtype)
* [getPlaceholder](_input_.input.md#getplaceholder)
* [getSnapshotBeforeUpdate](_input_.input.md#getsnapshotbeforeupdate)
* [hasError](_input_.input.md#haserror)
* [hasErrors](_input_.input.md#haserrors)
* [isValid](_input_.input.md#isvalid)
* [render](_input_.input.md#render)
* [shouldComponentUpdate](_input_.input.md#shouldcomponentupdate)
* [validate](_input_.input.md#validate)

---

## Constructors

<a id="constructor"></a>

###  constructor

⊕ **new Input**(props: *`IInputProps`*): [Input](_input_.input.md)

*Defined in Input.tsx:19*

Creates an instance of Input.

*__memberof__*: Input

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| props | `IInputProps` |  Props of the Input component |

**Returns:** [Input](_input_.input.md)

___

## Properties

<a id="errors"></a>

###  errors

**● errors**: *`IValidationError`[]*

*Inherited from ValidationComponent.errors*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:137*

Array with all the errors founded

*__type__*: {IValidationError\[\]}

*__memberof__*: ValidationComponent

___
<a id="fieldname"></a>

### `<Optional>` fieldName

**● fieldName**: *`undefined` \| `string`*

*Inherited from ValidationComponent.fieldName*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:144*

Define the name of the field input

*__type__*: {string}

*__memberof__*: ValidationComponent

___
<a id="locale"></a>

### `<Optional>` locale

**● locale**: *`undefined` \| `string`*

*Inherited from ValidationComponent.locale*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:130*

Define the language to show the messages

*__type__*: {string}

*__memberof__*: ValidationComponent

___
<a id="messages"></a>

###  messages

**● messages**: *`IValidationLocale`*

*Inherited from ValidationComponent.messages*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:151*

Array with all messages error

*__type__*: {IValidationLocale}

*__memberof__*: ValidationComponent

___
<a id="rules"></a>

###  rules

**● rules**: *`IValidationRules`*

*Inherited from ValidationComponent.rules*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:158*

Object to define the rules of field

*__type__*: {IValidationRules}

*__memberof__*: ValidationComponent

___
<a id="value"></a>

###  value

**● value**: *`TypeValidationValue`*

*Inherited from ValidationComponent.value*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:165*

Define the value of the field

*__type__*: {TypeValidationValue}

*__memberof__*: ValidationComponent

___

## Methods

<a id="iconerror"></a>

###  IconError

▸ **IconError**(): `TypeComponent`

*Defined in Input.tsx:186*

Method that return the Icon Error component

*__memberof__*: Input

**Returns:** `TypeComponent`

___
<a id="iconeye"></a>

###  IconEye

▸ **IconEye**(): `TypeComponent`

*Defined in Input.tsx:156*

Method that return the Icon Eye component toggle

*__memberof__*: Input

**Returns:** `TypeComponent`

___
<a id="iconleft"></a>

###  IconLeft

▸ **IconLeft**(): `TypeComponent`

*Defined in Input.tsx:116*

Method that return the Icon component to left

*__memberof__*: Input

**Returns:** `TypeComponent`

___
<a id="iconright"></a>

###  IconRight

▸ **IconRight**(): `TypeComponent`

*Defined in Input.tsx:136*

Method that return the Icon component to right

*__memberof__*: Input

**Returns:** `TypeComponent`

___
<a id="iconsuccess"></a>

###  IconSuccess

▸ **IconSuccess**(): `TypeComponent`

*Defined in Input.tsx:177*

Method that return the Icon Success component

*__memberof__*: Input

**Returns:** `TypeComponent`

___
<a id="input"></a>

###  Input

▸ **Input**(): `TypeComponent`

*Defined in Input.tsx:89*

Method that return the Input component

*__memberof__*: Input

**Returns:** `TypeComponent`

___
<a id="label"></a>

###  Label

▸ **Label**(): `TypeComponent`

*Defined in Input.tsx:74*

Method that return the Label component

*__memberof__*: Input

**Returns:** `TypeComponent`

___
<a id="messageinput"></a>

###  MessageInput

▸ **MessageInput**(): `TypeComponent`

*Defined in Input.tsx:195*

Method that return the message error or success

*__memberof__*: Input

**Returns:** `TypeComponent`

___
<a id="unsafe_componentwillmount"></a>

### `<Optional>` UNSAFE_componentWillMount

▸ **UNSAFE_componentWillMount**(): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@types/react/index.d.ts:638*

Called immediately before mounting occurs, and before `Component#render`. Avoid introducing any side-effects or subscriptions in this method.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use componentDidMount or the constructor instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Returns:** `void`

___
<a id="unsafe_componentwillreceiveprops"></a>

### `<Optional>` UNSAFE_componentWillReceiveProps

▸ **UNSAFE_componentWillReceiveProps**(nextProps: *`Readonly`<`IValidationProps` & `IInputProps`>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillReceiveProps*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@types/react/index.d.ts:670*

Called when the component may be receiving new props. React may call this even if props have not changed, so be sure to compare new and existing props if you only want to handle changes.

Calling `Component#setState` generally does not trigger this method.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use static getDerivedStateFromProps instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<`IValidationProps` & `IInputProps`> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="unsafe_componentwillupdate"></a>

### `<Optional>` UNSAFE_componentWillUpdate

▸ **UNSAFE_componentWillUpdate**(nextProps: *`Readonly`<`IValidationProps` & `IInputProps`>*, nextState: *`Readonly`<`IInputState`>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@types/react/index.d.ts:698*

Called immediately before rendering when new props or state is received. Not called for the initial render.

Note: You cannot call `Component#setState` here.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use getSnapshotBeforeUpdate instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<`IValidationProps` & `IInputProps`> |
| nextState | `Readonly`<`IInputState`> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="_iscomponent"></a>

### `<Private>` _isComponent

▸ **_isComponent**(kind: *`any`*): `boolean`

*Defined in Input.tsx:307*

Method to valid if a object is a component

*__memberof__*: Input

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| kind | `any` |  Object to validate |

**Returns:** `boolean`

___
<a id="_isprops"></a>

### `<Private>` _isProps

▸ **_isProps**(kind: *`any`*): `boolean`

*Defined in Input.tsx:318*

Method to valid if a object is a props component

*__memberof__*: Input

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| kind | `any` |  Object to validate |

**Returns:** `boolean`

___
<a id="_onchangevalue"></a>

### `<Private>` _onChangeValue

▸ **_onChangeValue**(value: *`any`*): `void`

*Defined in Input.tsx:287*

Method that fire when the text change to implement the validation

*__memberof__*: Input

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| value | `any` |  The text of the input |

**Returns:** `void`

___
<a id="_processprops"></a>

### `<Private>` _processProps

▸ **_processProps**(): `IInputProps`

*Defined in Input.tsx:328*

Method to process the props of the component

*__memberof__*: Input

**Returns:** `IInputProps`

___
<a id="_toggleeye"></a>

### `<Private>` _toggleEye

▸ **_toggleEye**(): `void`

*Defined in Input.tsx:276*

Toggle the state showPassword for the Icon Eye component

*__memberof__*: Input

**Returns:** `void`

___
<a id="componentdidcatch"></a>

### `<Optional>` componentDidCatch

▸ **componentDidCatch**(error: *`Error`*, errorInfo: *`ErrorInfo`*): `void`

*Inherited from ComponentLifecycle.componentDidCatch*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@types/react/index.d.ts:567*

Catches exceptions generated in descendant components. Unhandled exceptions will cause the entire component tree to unmount.

**Parameters:**

| Name | Type |
| ------ | ------ |
| error | `Error` |
| errorInfo | `ErrorInfo` |

**Returns:** `void`

___
<a id="componentdidmount"></a>

### `<Optional>` componentDidMount

▸ **componentDidMount**(): `void`

*Inherited from ComponentLifecycle.componentDidMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@types/react/index.d.ts:546*

Called immediately after a component is mounted. Setting state here will trigger re-rendering.

**Returns:** `void`

___
<a id="componentdidupdate"></a>

### `<Optional>` componentDidUpdate

▸ **componentDidUpdate**(prevProps: *`Readonly`<`IValidationProps` & `IInputProps`>*, prevState: *`Readonly`<`IInputState`>*, snapshot?: *[SS]()*): `void`

*Inherited from NewLifecycle.componentDidUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@types/react/index.d.ts:609*

Called immediately after updating occurs. Not called for the initial render.

The snapshot is only present if getSnapshotBeforeUpdate is present and returns non-null.

**Parameters:**

| Name | Type |
| ------ | ------ |
| prevProps | `Readonly`<`IValidationProps` & `IInputProps`> |
| prevState | `Readonly`<`IInputState`> |
| `Optional` snapshot | [SS]() |

**Returns:** `void`

___
<a id="componentwillmount"></a>

### `<Optional>` componentWillMount

▸ **componentWillMount**(): `void`

*Inherited from DeprecatedLifecycle.componentWillMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@types/react/index.d.ts:624*

Called immediately before mounting occurs, and before `Component#render`. Avoid introducing any side-effects or subscriptions in this method.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use componentDidMount or the constructor instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Returns:** `void`

___
<a id="componentwillreceiveprops"></a>

### `<Optional>` componentWillReceiveProps

▸ **componentWillReceiveProps**(nextProps: *`Readonly`<`IValidationProps` & `IInputProps`>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.componentWillReceiveProps*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@types/react/index.d.ts:653*

Called when the component may be receiving new props. React may call this even if props have not changed, so be sure to compare new and existing props if you only want to handle changes.

Calling `Component#setState` generally does not trigger this method.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use static getDerivedStateFromProps instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<`IValidationProps` & `IInputProps`> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="componentwillunmount"></a>

### `<Optional>` componentWillUnmount

▸ **componentWillUnmount**(): `void`

*Inherited from ComponentLifecycle.componentWillUnmount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@types/react/index.d.ts:562*

Called immediately before a component is destroyed. Perform any necessary cleanup in this method, such as cancelled network requests, or cleaning up any DOM elements created in `componentDidMount`.

**Returns:** `void`

___
<a id="componentwillupdate"></a>

### `<Optional>` componentWillUpdate

▸ **componentWillUpdate**(nextProps: *`Readonly`<`IValidationProps` & `IInputProps`>*, nextState: *`Readonly`<`IInputState`>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.componentWillUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@types/react/index.d.ts:683*

Called immediately before rendering when new props or state is received. Not called for the initial render.

Note: You cannot call `Component#setState` here.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use getSnapshotBeforeUpdate instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<`IValidationProps` & `IInputProps`> |
| nextState | `Readonly`<`IInputState`> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="geterror"></a>

###  getError

▸ **getError**(ruleName: *`string`*): `string` \| `null`

*Inherited from ValidationComponent.getError*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:203*

Find and return the field rule error

*__memberof__*: ValidationComponent

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| ruleName | `string` |  name of the rule |

**Returns:** `string` \| `null`

___
<a id="getkeyboardtype"></a>

###  getKeyboardType

▸ **getKeyboardType**(): `TypeInputKeyboard`

*Defined in Input.tsx:250*

Method that return the type of the keyboard

*__author__*: (Set the text for this tag by adding docthis.authorName to your settings file.)

*__memberof__*: Input

**Returns:** `TypeInputKeyboard`

___
<a id="getplaceholder"></a>

###  getPlaceholder

▸ **getPlaceholder**(): `string` \| `undefined`

*Defined in Input.tsx:236*

Method that return the text placeholder

*__memberof__*: Input

**Returns:** `string` \| `undefined`

___
<a id="getsnapshotbeforeupdate"></a>

### `<Optional>` getSnapshotBeforeUpdate

▸ **getSnapshotBeforeUpdate**(prevProps: *`Readonly`<`IValidationProps` & `IInputProps`>*, prevState: *`Readonly`<`IInputState`>*): `SS` \| `null`

*Inherited from NewLifecycle.getSnapshotBeforeUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@types/react/index.d.ts:603*

Runs before React applies the result of `render` to the document, and returns an object to be given to componentDidUpdate. Useful for saving things such as scroll position before `render` causes changes to it.

Note: the presence of getSnapshotBeforeUpdate prevents any of the deprecated lifecycle events from running.

**Parameters:**

| Name | Type |
| ------ | ------ |
| prevProps | `Readonly`<`IValidationProps` & `IInputProps`> |
| prevState | `Readonly`<`IInputState`> |

**Returns:** `SS` \| `null`

___
<a id="haserror"></a>

###  hasError

▸ **hasError**(ruleName: *`string`*): `boolean`

*Inherited from ValidationComponent.hasError*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:188*

Method to check if the field rule has error

*__memberof__*: ValidationComponent

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| ruleName | `string` |  name of the rule |

**Returns:** `boolean`

___
<a id="haserrors"></a>

###  hasErrors

▸ **hasErrors**(): `boolean`

*Inherited from ValidationComponent.hasErrors*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:180*

Method to check if the field has errors

*__memberof__*: ValidationComponent

**Returns:** `boolean`

___
<a id="isvalid"></a>

###  isValid

▸ **isValid**(): `boolean`

*Inherited from ValidationComponent.isValid*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:195*

Method to check if the field is valid

*__memberof__*: ValidationComponent

**Returns:** `boolean`

___
<a id="render"></a>

###  render

▸ **render**(): `TypeComponent`

*Defined in Input.tsx:35*

Method that renders the component

*__memberof__*: Input

**Returns:** `TypeComponent`

___
<a id="shouldcomponentupdate"></a>

### `<Optional>` shouldComponentUpdate

▸ **shouldComponentUpdate**(nextProps: *`Readonly`<`IValidationProps` & `IInputProps`>*, nextState: *`Readonly`<`IInputState`>*, nextContext: *`any`*): `boolean`

*Inherited from ComponentLifecycle.shouldComponentUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@types/react/index.d.ts:557*

Called to determine whether the change in props and state should trigger a re-render.

`Component` always returns true. `PureComponent` implements a shallow comparison on props and state and returns true if any props or states have changed.

If false is returned, `Component#render`, `componentWillUpdate` and `componentDidUpdate` will not be called.

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<`IValidationProps` & `IInputProps`> |
| nextState | `Readonly`<`IInputState`> |
| nextContext | `any` |

**Returns:** `boolean`

___
<a id="validate"></a>

###  validate

▸ **validate**(rules: *`IValidationInputRules`*): `boolean`

*Inherited from ValidationComponent.validate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Input/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:173*

Method to validate to verify if input respect the validation rules

*__memberof__*: ValidationComponent

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| rules | `IValidationInputRules` |  Object to define the rules of the input |

**Returns:** `boolean`

___

