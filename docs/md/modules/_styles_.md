[@ticmakers-react-native/input](../README.md) > ["styles"](../modules/_styles_.md)

# External module: "styles"

## Index

### Variables

* [styles](_styles_.md#styles)

### Object literals

* [colors](_styles_.md#colors)

---

## Variables

<a id="styles"></a>

### `<Const>` styles

**● styles**: *`object`* =  StyleSheet.create({
  containerItem: {
    alignItems: 'center',
    alignSelf: 'center',
    width: '100%',
  },

  borderError: {
    borderBottomColor: colors.error,
  },

  borderSuccess: {
    borderBottomColor: colors.success,
  },

  messages: {
    paddingLeft: 8,
    paddingRight: 8,
  },

  messagesError: {
    color: colors.error,
  },

  messagesSuccess: {
    color: colors.success,
  },
})

*Defined in styles.ts:8*

#### Type declaration

 borderError: `object`

 borderBottomColor: `string`

 borderSuccess: `object`

 borderBottomColor: `string`

 containerItem: `object`

 alignItems: "center"

 alignSelf: "center"

 width: `string`

 messages: `object`

 paddingLeft: `number`

 paddingRight: `number`

 messagesError: `object`

 color: `string`

 messagesSuccess: `object`

 color: `string`

___

## Object literals

<a id="colors"></a>

### `<Const>` colors

**colors**: *`object`*

*Defined in styles.ts:3*

<a id="colors.error"></a>

####  error

**● error**: *`string`* = "#e8333f"

*Defined in styles.ts:4*

___
<a id="colors.success"></a>

####  success

**● success**: *`string`* = "#3f8445"

*Defined in styles.ts:5*

___

___

