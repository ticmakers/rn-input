"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_native_1 = require("react-native");
var native_base_1 = require("native-base");
var core_1 = require("@ticmakers-react-native/core");
var icon_1 = require("@ticmakers-react-native/icon");
var styles_1 = require("./styles");
var Input = (function (_super) {
    __extends(Input, _super);
    function Input(props) {
        var _this = _super.call(this, props) || this;
        _this.state = _this._processProps();
        return _this;
    }
    Input.prototype.render = function () {
        var _a = this._processProps(), containerStyle = _a.containerStyle, disabled = _a.disabled, fixed = _a.fixed, floating = _a.floating, hideIconLeft = _a.hideIconLeft, hideIconRight = _a.hideIconRight, hideValidationIcons = _a.hideValidationIcons, inline = _a.inline, password = _a.password, regular = _a.regular, rounded = _a.rounded, stacked = _a.stacked;
        var _b = this.state, error = _b.error, success = _b.success;
        var itemProps = {
            disabled: disabled,
            error: error,
            regular: regular,
            rounded: rounded,
            success: success,
            fixedLabel: fixed,
            floatingLabel: floating,
            inlineLabel: inline,
            stackedLabel: stacked,
            style: [styles_1.default.containerItem],
        };
        return (React.createElement(react_native_1.View, { style: containerStyle },
            React.createElement(native_base_1.Item, __assign({}, itemProps),
                !hideIconLeft && this.IconLeft(),
                this.Label(),
                this.Input(),
                password ? this.IconEye() : (!hideIconRight && this.IconRight()),
                (success && !hideValidationIcons) && this.IconSuccess(),
                (error && !hideValidationIcons) && this.IconError()),
            this.MessageInput()));
    };
    Input.prototype.Label = function () {
        var _a = this._processProps(), fixed = _a.fixed, floating = _a.floating, inline = _a.inline, label = _a.label, stacked = _a.stacked;
        if (fixed || inline || floating || stacked) {
            return (React.createElement(native_base_1.Label, null, label));
        }
    };
    Input.prototype.Input = function () {
        var _this = this;
        var _a = this._processProps(), Component = _a.Component, password = _a.password;
        var _b = this.state, error = _b.error, success = _b.success, showPassword = _b.showPassword;
        if (Component) {
            return Component;
        }
        return (React.createElement(native_base_1.Input, __assign({}, this.props, this._processProps(), { keyboardType: this.getKeyboardType(), onChangeText: function (value) { return _this._onChangeValue(value); }, placeholder: this.getPlaceholder(), secureTextEntry: password && !showPassword, style: [
                success && styles_1.default.borderSuccess,
                error && styles_1.default.borderError,
            ] })));
    };
    Input.prototype.IconLeft = function () {
        var iconLeft = this.state.iconLeft;
        var icon;
        if (iconLeft) {
            if (this._isComponent(iconLeft)) {
                icon = iconLeft;
            }
            else if (this._isProps(iconLeft)) {
                icon = React.createElement(icon_1.default, __assign({}, iconLeft));
            }
        }
        return icon;
    };
    Input.prototype.IconRight = function () {
        var iconRight = this.state.iconRight;
        var icon;
        if (iconRight) {
            if (this._isComponent(iconRight)) {
                icon = iconRight;
            }
            else if (this._isProps(iconRight)) {
                icon = React.createElement(icon_1.default, __assign({}, iconRight));
            }
        }
        return icon;
    };
    Input.prototype.IconEye = function () {
        var showPassword = this.state.showPassword;
        var name = (showPassword ? 'eye-off' : 'eye');
        var iconProps = {
            name: name,
            containerStyle: { borderRadius: 100, overflow: 'hidden' },
            iconStyle: { padding: 3 },
            onPress: this._toggleEye.bind(this),
            type: 'material-community',
            underlayColor: 'rgba(0,0,0,.15)',
        };
        return React.createElement(icon_1.default, __assign({}, iconProps));
    };
    Input.prototype.IconSuccess = function () {
        return React.createElement(icon_1.default, { name: "check-circle", type: "font-awesome", color: styles_1.default.messagesSuccess.color });
    };
    Input.prototype.IconError = function () {
        return React.createElement(icon_1.default, { name: "times-circle", type: "font-awesome", color: styles_1.default.messagesError.color });
    };
    Input.prototype.MessageInput = function () {
        var _a = this._processProps(), msgError = _a.msgError, msgSuccess = _a.msgSuccess;
        var _b = this.state, error = _b.error, success = _b.success;
        if (success && msgSuccess) {
            return React.createElement(native_base_1.Text, { style: [styles_1.default.messages, styles_1.default.messagesSuccess] }, msgSuccess);
        }
        if (error) {
            var isDate = this.hasError('date');
            var isEmail = this.hasError('email');
            var isMaxLength = this.hasError('maxLength');
            var isMinLength = this.hasError('minLength');
            var isNumbers = this.hasError('numbers');
            var isRequired = this.hasError('required');
            var message = '';
            if (isRequired) {
                message = this.getError('required') || '';
            }
            else if (!isRequired && isMinLength) {
                message = this.getError('minLength') || '';
            }
            else if (!isRequired && isMaxLength) {
                message = this.getError('maxLength') || '';
            }
            else if ((!isRequired && !isMinLength) && (isEmail || isDate || isNumbers)) {
                if (isDate) {
                    message = this.getError('date') || '';
                }
                else if (isEmail) {
                    message = this.getError('email') || '';
                }
                else if (isNumbers) {
                    message = this.getError('numbers') || '';
                }
            }
            return React.createElement(native_base_1.Text, { style: [styles_1.default.messages, styles_1.default.messagesError] }, msgError || message);
        }
    };
    Input.prototype.getPlaceholder = function () {
        var _a = this._processProps(), fixed = _a.fixed, floating = _a.floating, inline = _a.inline, label = _a.label, stacked = _a.stacked;
        if ((!fixed && !inline && !floating && !stacked)) {
            return label;
        }
    };
    Input.prototype.getKeyboardType = function () {
        var _a = this._processProps(), email = _a.email, number = _a.number, tel = _a.tel;
        var keyboardType = this.props.keyboardType;
        var type = 'default';
        if (keyboardType) {
            return keyboardType;
        }
        if (number) {
            type = 'number-pad';
        }
        else if (email) {
            type = 'email-address';
        }
        else if (tel) {
            type = 'phone-pad';
        }
        return type;
    };
    Input.prototype._toggleEye = function () {
        this.setState({ showPassword: !this.state.showPassword });
    };
    Input.prototype._onChangeValue = function (value) {
        var rules = this._processProps().rules;
        var onChangeText = this.props.onChangeText;
        this.value = value;
        this.validate(rules || {});
        this.setState({ value: value, error: this.hasErrors(), success: !this.hasErrors() });
        if (onChangeText) {
            return onChangeText(value);
        }
    };
    Input.prototype._isComponent = function (kind) {
        return (kind._owner && kind._owner.constructor.name === 'FiberNode' && kind.$$typeof && kind.$$typeof.constructor.name === 'Symbol');
    };
    Input.prototype._isProps = function (kind) {
        return (!this._isComponent(kind) && Object.keys(kind).length > 0);
    };
    Input.prototype._processProps = function () {
        var _a = this.props, Component = _a.Component, containerStyle = _a.containerStyle, disabled = _a.disabled, error = _a.error, fixed = _a.fixed, floating = _a.floating, hideIconLeft = _a.hideIconLeft, hideIconRight = _a.hideIconRight, hideValidationIcons = _a.hideValidationIcons, iconLeft = _a.iconLeft, iconRight = _a.iconRight, inline = _a.inline, label = _a.label, lang = _a.lang, msgError = _a.msgError, msgSuccess = _a.msgSuccess, number = _a.number, options = _a.options, password = _a.password, regular = _a.regular, rounded = _a.rounded, rules = _a.rules, showPassword = _a.showPassword, stacked = _a.stacked, success = _a.success, tel = _a.tel, value = _a.value;
        var props = {
            Component: (options && options.Component) || (Component || undefined),
            containerStyle: (options && options.containerStyle) || (containerStyle || undefined),
            disabled: (options && options.disabled) || (disabled || false),
            error: (options && options.error) || (error || false),
            fixed: (options && options.fixed) || (fixed || false),
            floating: (options && options.floating) || (floating || false),
            hideIconLeft: (options && options.hideIconLeft) || (hideIconLeft || false),
            hideIconRight: (options && options.hideIconRight) || (hideIconRight || false),
            hideValidationIcons: (options && options.hideValidationIcons) || (hideValidationIcons || false),
            iconLeft: (options && options.iconLeft) || (iconLeft || undefined),
            iconRight: (options && options.iconRight) || (iconRight || undefined),
            inline: (options && options.inline) || (inline || false),
            label: (options && options.label) || (label || undefined),
            lang: (options && options.lang) || (lang || 'en'),
            msgError: (options && options.msgError) || (msgError || undefined),
            msgSuccess: (options && options.msgSuccess) || (msgSuccess || undefined),
            number: (options && options.number) || (number || false),
            password: (options && options.password) || (password || false),
            regular: (options && options.regular) || (regular || false),
            rounded: (options && options.rounded) || (rounded || false),
            rules: (options && options.rules) || (rules || undefined),
            showPassword: (options && options.showPassword) || (showPassword || false),
            stacked: (options && options.stacked) || (stacked || false),
            success: (options && options.success) || (success || false),
            tel: (options && options.tel) || (tel || false),
            value: (options && options.value) || (value || undefined),
        };
        this.fieldName = props.label;
        this.value = props.value;
        this.locale = props.lang;
        return props;
    };
    return Input;
}(core_1.ValidationComponent));
exports.default = Input;
//# sourceMappingURL=Input.js.map