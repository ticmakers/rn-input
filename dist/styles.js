"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var react_native_1 = require("react-native");
var colors = {
    error: '#e8333f',
    success: '#3f8445',
};
var styles = react_native_1.StyleSheet.create({
    containerItem: {
        alignItems: 'center',
        alignSelf: 'center',
        width: '100%',
    },
    borderError: {
        borderBottomColor: colors.error,
    },
    borderSuccess: {
        borderBottomColor: colors.success,
    },
    messages: {
        paddingLeft: 8,
        paddingRight: 8,
    },
    messagesError: {
        color: colors.error,
    },
    messagesSuccess: {
        color: colors.success,
    },
});
exports.default = styles;
//# sourceMappingURL=styles.js.map