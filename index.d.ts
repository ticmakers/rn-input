import * as React from 'react'
import {
  RecursiveArray,
  RegisteredStyle,
  StyleProp,
  TextInputProps,
  ViewStyle,
} from 'react-native'

import { ValidationComponent } from '@ticmakers-react-native/core'
import { IIconProps } from '@ticmakers-react-native/icon'

/**
 * Type of keyboard to handle in the Input component (Android & iOS)
 */
export type TypeInputKeyboardAcross =
  'default'
  | 'decimal-pad'
  | 'email-address'
  | 'number-pad'
  | 'numeric'
  | 'phone-pad'

/**
 * Type of keyboard to handle in the Input component (iOS)
 */
export type TypeInputKeyboardIOS =
  'ascii-capable'
  | 'numbers-and-punctuation'
  | 'url'
  | 'name-phone-pad'
  | 'twitter'
  | 'web-search'

/**
 * Type of keyboard to handle in the Input component (Android)
 */
export type TypeInputKeyboardAndroid = 'visible-password'

/**
 * Type of keyboard to handle in the Input component
 */
export type TypeInputKeyboard = TypeInputKeyboardAcross | TypeInputKeyboardIOS | TypeInputKeyboardAndroid

/**
 * Type of component to typing components in the Input component
 */
export type TypeComponent =
  JSX.Element
  | React.ComponentClass
  | false
  | null
  | undefined

/**
 * Type to define the prop style of the Input component
 */
export type TypeStyle =
  false
  | ViewStyle
  | RegisteredStyle<ViewStyle>
  | RecursiveArray<false | ViewStyle | RegisteredStyle<ViewStyle> | null | undefined>
  | StyleProp<ViewStyle>[]
  | null
  | undefined

/**
 * Type to define the languages available to show the message in the Input component
 */
export type TypeLanguages = 'en' | 'es' | 'fr' | 'fa'

/**
 * Interface to define the type of icon in the Input component
 * @interface IIconType
 */
export interface IIconType {
  $$typeof?: Symbol
  _owner?: {
    tag: number | string
    key: number | string | null
    props: {}
  }
  _store?: object
  key?: number | string | null
  name?: string
  props?: object
  ref?: number | string | null
  style?: object
  type?: () => {}
}

/**
 * Interface to define the rules of the Input component
 * @interface IInputRules
 */
export interface IInputRules {
  date?: boolean | string | RegExp
  email?: boolean | RegExp
  maxLength?: number
  minLength?: number
  numbers?: boolean | RegExp
  pattern?: RegExp
  required?: true
}

/**
 * Interface to define the states of the Input component
 * @export
 * @interface IInputState
 */
export interface IInputState extends TextInputProps {
  /**
   * Component to replace the Input component by default
   * @type {TypeComponent}
   * @memberof IInputState
   */
  Component?: TypeComponent

  /**
   * Apply styles to the container of the Input component
   * @type {TypeStyle}
   * @memberof IInputState
   */
  containerStyle?: TypeStyle

  /**
   * Disabled the Input component
   * @type {boolean}
   * @memberof IInputState
   * @default false
   */
  disabled?: boolean

  /**
   * Set true to define the input as email
   * @type {boolean}
   * @memberof IInputState
   * @default false
   */
  email?: boolean

  /**
   * Set true to show error style in the Input component
   * @type {boolean}
   * @memberof IInputState
   * @default false
   */
  error?: boolean

  /**
   * Set true to define type label as fixed
   * @type {boolean}
   * @memberof IInputState
   * @default false
   */
  fixed?: boolean

  /**
   * Set true to define type label as floating
   * @type {boolean}
   * @memberof IInputState
   * @default false
   */
  floating?: boolean

  /**
   * Set true to hide the icon left of the Input component
   * @type {boolean}
   * @memberof IInputState
   * @default false
   */
  hideIconLeft?: boolean

  /**
   * Set true to hide the icon right of the Input component
   * @type {boolean}
   * @memberof IInputState
   * @default false
   */
  hideIconRight?: boolean

  /**
   * Set true to hide the icons that show when validate the Input component
   * @type {boolean}
   * @memberof IInputState
   * @default false
   */
  hideValidationIcons?: boolean

  /**
   * Define the icons props or define a Component to show the icon left
   * @type {(IIconProps | TypeComponent)}
   * @memberof IInputState
   */
  iconLeft?: IIconProps | TypeComponent

  /**
   * Define the icons props or define a Component to show the icon right
   * @type {(IIconProps | TypeComponent)}
   * @memberof IInputState
   */
  iconRight?: IIconProps | TypeComponent

  /**
   * Set true to define type label as inline
   * @type {boolean}
   * @memberof IInputState
   * @default false
   */
  inline?: boolean

  /**
   * Define a text label to the Input component
   * @type {string}
   * @memberof IInputState
   */
  label?: string

  /**
   * Define the language to show the messages error
   * @type {TypeLanguages}
   * @memberof IInputState
   * @default en
   */
  lang?: TypeLanguages

  /**
   * Define a text message to show when the input has errors
   * @type {string}
   * @memberof IInputState
   */
  msgError?: string

  /**
   * Define a text message to show when the input has not errors
   * @type {string}
   * @memberof IInputState
   */
  msgSuccess?: string

  /**
   * Set true to define the input as number
   * @type {boolean}
   * @memberof IInputState
   * @default false
   */
  number?: boolean

  /**
   * Set true to define the input as password
   * @type {boolean}
   * @memberof IInputState
   * @default false
   */
  password?: boolean

  /**
   * Set true to define a regular style to the Input component
   * @type {boolean}
   * @memberof IInputState
   * @default false
   */
  regular?: boolean

  /**
   * set true to define a rounded style to the Input component
   * @type {boolean}
   * @memberof IInputState
   * @default false
   */
  rounded?: boolean

  /**
   * Define a object with the rules to validate the Input component
   * @type {IInputRules}
   * @memberof IInputState
   */
  rules?: IInputRules

  /**
   * Set true to show text password
   * @type {boolean}
   * @memberof IInputState
   * @default false
   */
  showPassword?: boolean

  /**
   * Set true to define type label as stacked
   * @type {boolean}
   * @memberof IInputState
   */
  stacked?: boolean

  /**
   * Set true to show success style in the Input component
   * @type {boolean}
   * @memberof IInputState
   * @default false
   */
  success?: boolean

  /**
   * Set true to define the input as telephone
   * @type {boolean}
   * @memberof IInputState
   * @default false
   */
  tel?: boolean
}

/**
 * Interface to define the props of the Input component
 * @export
 * @interface IInputProps
 * @extends {IInputState}
 */
export interface IInputProps extends IInputState {
  /**
   * Prop for group all the props of the Input component
   */
  options?: IInputState
}

/**
 * Input Component to write text, email, number and password
 * @class Input
 * @extends {ValidationComponent<IInputProps, IInputState>}
 */
declare class Input extends ValidationComponent<IInputProps, IInputState> {
  /**
   * Method that renders the component
   * @returns {TypeComponent}
   * @memberof Input
   */
  public render(): TypeComponent

  /**
   * Method that return the Label component
   * @returns {TypeComponent}
   * @memberof Input
   */
  public Label(): TypeComponent

  /**
   * Method that return the Input component
   * @returns {TypeComponent}
   * @memberof Input
   */
  public Input(): TypeComponent

  /**
   * Method that return the Icon component to left
   * @returns {TypeComponent}
   * @memberof Input
   */
  public IconLeft(): TypeComponent

  /**
   * Method that return the Icon component to right
   * @returns {TypeComponent}
   * @memberof Input
   */
  public IconRight(): TypeComponent

  /**
   * Method that return the Icon Eye component toggle
   * @returns {TypeComponent}
   * @memberof Input
   */
  public IconEye(): TypeComponent

  /**
   * Method that return the Icon Success component
   * @returns {TypeComponent}
   * @memberof Input
   */
  public IconSuccess(): TypeComponent

  /**
   * Method that return the Icon Error component
   * @returns {TypeComponent}
   * @memberof Input
   */
  public IconError(): TypeComponent

  /**
   * Method that return the message error or success
   * @returns {TypeComponent}
   * @memberof Input
   */
  public MessageInput(): TypeComponent

  /**
   * Method that return the text placeholder
   * @returns {(string | undefined)}
   * @memberof Input
   */
  public getPlaceholder(): string | undefined

  /**
   * Method that return the type of the keyboard
   * @author (Set the text for this tag by adding docthis.authorName to your settings file.)
   * @returns {TypeInputKeyboard}
   * @memberof Input
   */
  public getKeyboardType(): TypeInputKeyboard

  /**
   * Toggle the state showPassword for the Icon Eye component
   * @private
   * @returns {void}
   * @memberof Input
   */
  private _toggleEye(): void

  /**
   * Method that fire when the text change to implement the validation
   * @private
   * @param {*} value The text of the input
   * @returns {void}
   * @memberof Input
   */
  private _onChangeValue(value: any): void

  /**
   * Method to valid if a object is a component
   * @private
   * @param {*} kind Object to validate
   * @returns {boolean}
   * @memberof Input
   */
  private _isComponent(kind: any): boolean

  /**
   * Method to valid if a object is a props component
   * @private
   * @param {*} kind Object to validate
   * @returns {boolean}
   * @memberof Input
   */
  private _isProps(kind: any): boolean

  /**
   * Method to process the props of the component
   * @private
   * @returns {IInputProps}
   * @memberof Input
   */
  private _processProps(): IInputProps
}

/**
 * Declaration to module
 */
declare module '@ticmakers-react-native/input'

export default Input
