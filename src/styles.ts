import { StyleSheet } from 'react-native'

const colors = {
  error: '#e8333f',
  success: '#3f8445',
}

const styles = StyleSheet.create({
  containerItem: {
    alignItems: 'center',
    alignSelf: 'center',
    width: '100%',
  },

  borderError: {
    borderBottomColor: colors.error,
  },

  borderSuccess: {
    borderBottomColor: colors.success,
  },

  messages: {
    paddingLeft: 8,
    paddingRight: 8,
  },

  messagesError: {
    color: colors.error,
  },

  messagesSuccess: {
    color: colors.success,
  },
})

export default styles
