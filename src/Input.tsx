import * as React from 'react'
import { View } from 'react-native'
import { Item, Input as NB_Input, Label, Text } from 'native-base'

import { ValidationComponent } from '@ticmakers-react-native/core'
import Icon, { IIconProps } from '@ticmakers-react-native/icon'

// Interfaces
import { IInputProps, IInputState, TypeInputKeyboard, TypeComponent } from './../index'

// Themes
import styles from './styles'

/**
 * Input Component to write text, email, number and password
 * @class Input
 * @extends {ValidationComponent<IInputProps, IInputState>}
 */
export default class Input extends ValidationComponent<IInputProps, IInputState> {
  /**
   * Creates an instance of Input.
   * @param {IInputProps} props Props of the Input component
   * @memberof Input
   */
  constructor(props: IInputProps) {
    super(props)
    this.state = this._processProps()
  }

  /**
   * Method that renders the component
   * @returns {TypeComponent}
   * @memberof Input
   */
  public render(): TypeComponent {
    const { containerStyle, disabled, fixed, floating, hideIconLeft, hideIconRight, hideValidationIcons, inline, password, regular, rounded, stacked } = this._processProps()
    const { error, success } = this.state

    const itemProps = {
      disabled,
      error,
      regular,
      rounded,
      success,
      // tslint:disable-next-line: object-literal-sort-keys
      fixedLabel: fixed,
      floatingLabel: floating,
      inlineLabel: inline,
      stackedLabel: stacked,
      style: [styles.containerItem],
    }

    return (
      <View style={ containerStyle }>
        <Item { ...itemProps }>
          { !hideIconLeft && this.IconLeft() }
          { this.Label() }
          { this.Input() }
          { password ? this.IconEye() : (!hideIconRight && this.IconRight()) }
          { (success && !hideValidationIcons) && this.IconSuccess() }
          { (error && !hideValidationIcons) && this.IconError() }
        </Item>

        { this.MessageInput() }
      </View>
    )
  }

  /**
   * Method that return the Label component
   * @returns {TypeComponent}
   * @memberof Input
   */
  public Label(): TypeComponent {
    const { fixed, floating, inline, label, stacked } = this._processProps()

    if (fixed || inline || floating || stacked) {
      return (
        <Label>{ label }</Label>
      )
    }
  }

  /**
   * Method that return the Input component
   * @returns {TypeComponent}
   * @memberof Input
   */
  public Input(): TypeComponent {
    const { Component, password } = this._processProps()
    const { error, success, showPassword } = this.state

    if (Component) { return Component }

    return (
      <NB_Input
        { ...this.props }
        { ...this._processProps() }
        keyboardType={ this.getKeyboardType() }
        onChangeText={ value => this._onChangeValue(value) }
        placeholder={ this.getPlaceholder() }
        secureTextEntry={ password && !showPassword }
        style={[
          success && styles.borderSuccess,
          error && styles.borderError,
        ]}
      />
    )
  }

  /**
   * Method that return the Icon component to left
   * @returns {TypeComponent}
   * @memberof Input
   */
  public IconLeft(): TypeComponent {
    const { iconLeft } = this.state
    let icon: TypeComponent

    if (iconLeft) {
      if (this._isComponent(iconLeft)) {
        icon = iconLeft as TypeComponent
      } else if (this._isProps(iconLeft)) {
        icon = <Icon { ...iconLeft as IIconProps } />
      }
    }

    return icon
  }

  /**
   * Method that return the Icon component to right
   * @returns {TypeComponent}
   * @memberof Input
   */
  public IconRight(): TypeComponent {
    const { iconRight } = this.state
    let icon: TypeComponent

    if (iconRight) {
      if (this._isComponent(iconRight)) {
        icon = iconRight as TypeComponent
      } else if (this._isProps(iconRight)) {
        icon = <Icon { ...iconRight as IIconProps } />
      }
    }

    return icon
  }

  /**
   * Method that return the Icon Eye component toggle
   * @returns {TypeComponent}
   * @memberof Input
   */
  public IconEye(): TypeComponent {
    const { showPassword } = this.state
    const name = (showPassword ? 'eye-off' : 'eye')
    const iconProps: IIconProps = {
      name,
      // tslint:disable-next-line: object-literal-sort-keys
      containerStyle: { borderRadius: 100, overflow: 'hidden' },
      iconStyle: { padding: 3 },
      onPress: this._toggleEye.bind(this),
      type: 'material-community',
      underlayColor: 'rgba(0,0,0,.15)',
    }

    return <Icon { ...iconProps } />
  }

  /**
   * Method that return the Icon Success component
   * @returns {TypeComponent}
   * @memberof Input
   */
  public IconSuccess(): TypeComponent {
    return <Icon name="check-circle" type="font-awesome" color={ styles.messagesSuccess.color } />
  }

  /**
   * Method that return the Icon Error component
   * @returns {TypeComponent}
   * @memberof Input
   */
  public IconError(): TypeComponent {
    return <Icon name="times-circle" type="font-awesome" color={ styles.messagesError.color } />
  }

  /**
   * Method that return the message error or success
   * @returns {TypeComponent}
   * @memberof Input
   */
  public MessageInput(): TypeComponent {
    const { msgError, msgSuccess } = this._processProps()
    const { error, success } = this.state

    if (success && msgSuccess) {
      return <Text style={ [styles.messages, styles.messagesSuccess] }>{ msgSuccess }</Text>
    }

    if (error) {
      const isDate = this.hasError('date')
      const isEmail = this.hasError('email')
      const isMaxLength = this.hasError('maxLength')
      const isMinLength = this.hasError('minLength')
      const isNumbers = this.hasError('numbers')
      const isRequired = this.hasError('required')
      let message = ''

      if (isRequired) {
        message = this.getError('required') || ''
      } else if (!isRequired && isMinLength) {
        message = this.getError('minLength') || ''
      } else if (!isRequired && isMaxLength) {
        message = this.getError('maxLength') || ''
      } else if ((!isRequired && !isMinLength) && (isEmail || isDate || isNumbers)) {
        if (isDate) {
          message = this.getError('date') || ''
        } else if (isEmail) {
          message = this.getError('email') || ''
        } else if (isNumbers) {
          message = this.getError('numbers') || ''
        }
      }
      return <Text style={ [styles.messages, styles.messagesError] }>{ msgError || message }</Text>
    }
  }

  /**
   * Method that return the text placeholder
   * @returns {(string | undefined)}
   * @memberof Input
   */
  public getPlaceholder(): string | undefined {
    const { fixed, floating, inline, label, stacked } = this._processProps()

    if ((!fixed && !inline && !floating && !stacked)) {
      return label
    }
  }

  /**
   * Method that return the type of the keyboard
   * @author (Set the text for this tag by adding docthis.authorName to your settings file.)
   * @returns {TypeInputKeyboard}
   * @memberof Input
   */
  public getKeyboardType(): TypeInputKeyboard {
    // tslint:disable-next-line: variable-name
    const { email, number, tel } = this._processProps()
    const { keyboardType } = this.props
    let type: TypeInputKeyboard = 'default'

    if (keyboardType) { return keyboardType }

    if (number) {
      // type = 'numeric'
      type = 'number-pad'
    } else if (email) {
      type = 'email-address'
    } else if (tel) {
      type = 'phone-pad'
    }

    return type
  }

  /**
   * Toggle the state showPassword for the Icon Eye component
   * @private
   * @returns {void}
   * @memberof Input
   */
  private _toggleEye(): void {
    this.setState({ showPassword: !this.state.showPassword })
  }

  /**
   * Method that fire when the text change to implement the validation
   * @private
   * @param {*} value The text of the input
   * @returns {void}
   * @memberof Input
   */
  private _onChangeValue(value: any): void {
    const { rules } = this._processProps()
    const { onChangeText } = this.props

    this.value = value
    this.validate(rules || {})
    this.setState({ value, error: this.hasErrors(), success: !this.hasErrors() })

    if (onChangeText) {
      return onChangeText(value)
    }
  }

  /**
   * Method to valid if a object is a component
   * @private
   * @param {*} kind Object to validate
   * @returns {boolean}
   * @memberof Input
   */
  private _isComponent(kind: any): boolean {
    return (kind._owner && kind._owner.constructor.name === 'FiberNode' && kind.$$typeof && kind.$$typeof.constructor.name === 'Symbol')
  }

  /**
   * Method to valid if a object is a props component
   * @private
   * @param {*} kind Object to validate
   * @returns {boolean}
   * @memberof Input
   */
  private _isProps(kind: any): boolean {
    return (!this._isComponent(kind) && Object.keys(kind).length > 0)
  }

  /**
   * Method to process the props of the component
   * @private
   * @returns {IInputProps}
   * @memberof Input
   */
  private _processProps(): IInputProps {
    // tslint:disable-next-line: variable-name
    const { Component, containerStyle, disabled, error, fixed, floating, hideIconLeft, hideIconRight, hideValidationIcons, iconLeft, iconRight, inline,  label, lang, msgError, msgSuccess, number, options, password, regular, rounded, rules, showPassword, stacked, success, tel, value } = this.props

    const props: IInputProps = {
      Component: (options && options.Component) || (Component || undefined),
      containerStyle: (options && options.containerStyle) || (containerStyle || undefined),
      disabled: (options && options.disabled) || (disabled || false),
      error: (options && options.error) || (error || false),
      fixed: (options && options.fixed) || (fixed || false),
      floating: (options && options.floating) || (floating || false),
      hideIconLeft: (options && options.hideIconLeft) || (hideIconLeft || false),
      hideIconRight: (options && options.hideIconRight) || (hideIconRight || false),
      hideValidationIcons: (options && options.hideValidationIcons) || (hideValidationIcons || false),
      iconLeft: (options && options.iconLeft) || (iconLeft || undefined),
      iconRight: (options && options.iconRight) || (iconRight || undefined),
      inline: (options && options.inline) || (inline || false),
      label: (options && options.label) || (label || undefined),
      lang: (options && options.lang) || (lang || 'en'),
      msgError: (options && options.msgError) || (msgError || undefined),
      msgSuccess: (options && options.msgSuccess) || (msgSuccess || undefined),
      number: (options && options.number) || (number || false),
      password: (options && options.password) || (password || false),
      regular: (options && options.regular) || (regular || false),
      rounded: (options && options.rounded) || (rounded || false),
      rules: (options && options.rules) || (rules || undefined),
      showPassword: (options && options.showPassword) || (showPassword || false),
      stacked: (options && options.stacked) || (stacked || false),
      success: (options && options.success) || (success || false),
      tel: (options && options.tel) || (tel || false),
      value: (options && options.value) || (value || undefined),
    }

    this.fieldName = props.label
    this.value = props.value
    this.locale = props.lang

    return props
  }
}
